'use strict';
const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const KINESIS = new AWS.Kinesis();
const BUCKET_NAME = 'chain-kinesis-bucket';
const STREAM_NAME = 'chain-kinesis-stream'

module.exports.pushEvent = async event => {
    console.log(event);

    const fileObject = event["Records"][0];
    const fileName = fileObject["s3"]["object"]["key"];

    console.log(fileObject);
    console.log(fileName);

    return getDataFromS3(fileName)
        .then(data => sendDataToKinesis(data))
        .catch(err => console.error(err, err.stack));
}

async function sendDataToKinesis(data) {
    const params = {
        Data: data,
        PartitionKey: '1',
        StreamName: STREAM_NAME
    };

    return new Promise((resolve, reject) => {
        KINESIS.putRecord(params, (err, data) => {
            if (err) {
                console.error(err, err.stack);
                reject(err);
                return err;
            }

            resolve();
        });
    });
}

async function getDataFromS3(fileName) {
    const getParams = {
        Bucket: BUCKET_NAME,
        Key: fileName
    }

    return new Promise((resolve, reject) => {
        S3.getObject(getParams, (err, data) => {
            if (err) {
                console.error(err, err.stack);
                reject(err);
                return err;
            }

            const objectData = data.Body;
            console.log(objectData);

            resolve(objectData);
        });
    });
}
